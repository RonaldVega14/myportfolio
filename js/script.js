const spans = document.querySelectorAll('h1 span')
spans.forEach(span => span.addEventListener('mouseover', function (e) {
    span.classList.add('animated', 'rubberBand')
}))
spans.forEach(span => span.addEventListener('mouseout', function (e) {
    span.classList.remove('animated', 'rubberBand')
}))

const htmlbar = document.querySelector('.bar-html')
const cssbar = document.querySelector('.bar-css')
const jsbar = document.querySelector('.bar-javascript')
const flutterbar = document.querySelector('.bar-flutter')
const javabar = document.querySelector('.bar-java')
const angularbar = document.querySelector('.bar-angular')
const sqlbar = document.querySelector('.bar-sql')
const nosqlbar = document.querySelector('.bar-nosql')

var t1 = new TimelineLite();

t1.fromTo(flutterbar, .75, { width: 'calc(0% - 6px)' }, { width: 'calc(98% - 6px)', ease: Power4.easeout })
    .fromTo(javabar, .75, { width: 'calc(0% - 6px)' }, { width: 'calc(70% - 6px)', ease: Power4.easeout })
    .fromTo(angularbar, .75, { width: 'calc(0% - 6px)' }, { width: 'calc(85% - 6px)', ease: Power4.easeout })
    .fromTo(htmlbar, .75, { width: 'calc(0% - 6px)' }, { width: 'calc(90% - 6px)', ease: Power4.easeout })
    .fromTo(cssbar, .75, { width: 'calc(0% - 6px)' }, { width: 'calc(85% - 6px)', ease: Power4.easeout })
    .fromTo(jsbar, .75, { width: 'calc(0% - 6px)' }, { width: 'calc(75% - 6px)', ease: Power4.easeout })
    .fromTo(sqlbar, .75, { width: 'calc(0% - 6px)' }, { width: 'calc(50% - 6px)', ease: Power4.easeout })
    .fromTo(nosqlbar, .75, { width: 'calc(0% - 6px)' }, { width: 'calc(55% - 6px)', ease: Power4.easeout })

const controller = new ScrollMagic.Controller()
const scene = new ScrollMagic.Scene({
    triggerElement: '.skills',
    triggerHook: 0.5
})
    .setTween(t1)
    .addTo(controller)

const showRequiredCategory = event => {
    const getId = event.id
    const links = document.querySelectorAll('.work-category button')
    for (i = 0; i < links.length; i++) {
        if (links[i].hasAttribute('class')) {
            links[i].classList.remove('active')
        }
    }
    var category = '.category-' + getId;
    event.classList.add('active')
    const getCategory = document.querySelector(category)
    const categories = document.querySelectorAll('div[class ^= "category-"]')
    categories.forEach(function (cat) {
        if (cat.hasAttribute('class')) {
            cat.classList.remove('showCategory')
            cat.classList.add('hideCategory')
        }
    })

    getCategory.classList.remove('hideCategory')
    getCategory.classList.add('showCategory')
}